/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: ['./src/**/*.{js,ts,jsx,tsx}'],
    theme: {
        colors: {
            taupe: '#B29379',
            font: '#2F2F2F',
            'font-light': '#8F8F8F'
        },
        extend: {
            fontFamily: {
                sans: ['Raleway', ...defaultTheme.fontFamily.sans]
            }
        }
    },
    plugins: []
};
