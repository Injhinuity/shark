import AppContainer from 'components/app.container';
import Head from 'next/head';

export default function Home() {
    return (
        <AppContainer>
            <>
                <Head>
                    <title>Shark - Leagues</title>
                    <meta
                        name="description"
                        content="OSRS Stat tracker for leagues"
                    />
                    <link rel="icon" href="/favicon.ico" />
                </Head>
            </>
        </AppContainer>
    );
}
