import AppContainer from 'components/app.container';
import Head from 'next/head';

export default function Settings() {
    return (
        <AppContainer>
            <>
                <Head>
                    <title>Shark - Settings</title>
                    <meta
                        name="description"
                        content="OSRS Stat tracker settings management"
                    />
                    <link rel="icon" href="/favicon.ico" />
                </Head>
            </>
        </AppContainer>
    );
}
