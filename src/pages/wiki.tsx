import AppContainer from 'components/app.container';
import Head from 'next/head';

export default function Wiki() {
    return (
        <AppContainer>
            <>
                <Head>
                    <title>Shark - Wiki</title>
                    <meta
                        name="description"
                        content="OSRS Stat tracker wiki page"
                    />
                    <link rel="icon" href="/favicon.ico" />
                </Head>
            </>
        </AppContainer>
    );
}
