import AppContainer from 'components/app.container';
import Head from 'next/head';

export default function Home() {
    return (
        <AppContainer>
            <>
                <Head>
                    <title>Shark - Group</title>
                    <meta
                        name="description"
                        content="OSRS Stat tracker for group ironman"
                    />
                    <link rel="icon" href="/favicon.ico" />
                </Head>
            </>
        </AppContainer>
    );
}
