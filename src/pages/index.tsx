import AppContainer from 'components/app.container';
import Head from 'next/head';

export default function Home() {
    return (
        <AppContainer>
            <>
                <Head>
                    <title>Shark - Home</title>
                    <meta
                        name="description"
                        content="OSRS Stat tracker home page"
                    />
                    <link rel="icon" href="/favicon.ico" />
                </Head>
                <Dashboard />
            </>
        </AppContainer>
    );
}

const Dashboard = () => {
    return (
        <>
            <div className="flex flex-row flex-grow">
                <MainDataView />
                <SubDataView />
            </div>
            <div>
                <LowerDataView />
            </div>
        </>
    );
};

const MainDataView = () => {
    return (
        <div className="flex flex-col flex-grow">
            <div className="flex flex-row items-center mb-12">
                <Title />
                <SearchBar />
            </div>
            <DataView />
        </div>
    );
};

const SubDataView = () => {
    return (
        <div className="flex flex-col">
            <div className="text-3xl text-font">Search History</div>
            <div>Search list</div>
        </div>
    );
};

const LowerDataView = () => {
    return (
        <div className="flex flex-col">
            <div className="text-3xl text-font">Recommended</div>
            <div className="flex flex-row">Cards</div>
        </div>
    );
};

const Title = () => {
    return (
        <div className="flex flex-col">
            <div className="text-5xl text-font">Shark</div>
            <div className="text-2xl font-light text-font-light">
                Cooked and ready
            </div>
        </div>
    );
};

const SearchBar = () => {
    return <div>Search bar</div>;
};

const DataView = () => {
    return <div className="flex flex-grow ">Data view</div>;
};
