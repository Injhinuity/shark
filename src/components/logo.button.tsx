import Image from 'next/image';
import Link from 'next/link';
import logo from 'public/images/logo.png';

const LogoButton: React.FC = () => {
    return (
        <Link href="/">
            <a
                className="block rounded-3xl p-2 transition-all duration-200 hover:cursor-pointer hover:bg-font hover:bg-opacity-25
                            active:bg-font active:bg-opacity-20"
            >
                <Image
                    src={logo}
                    width="80"
                    height="80"
                    layout="fixed"
                    alt="Shark logo"
                />
            </a>
        </Link>
    );
};

export default LogoButton;
