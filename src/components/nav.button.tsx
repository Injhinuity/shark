import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

type Props = {
    href: string;
    children: JSX.Element;
};

const NavButton: React.FC<Props> = ({ href, children }) => {
    const router = useRouter();

    const resolveTextColor = () =>
        router.asPath == href ? 'text-taupe' : 'text-font-light';

    return (
        <Link href={href} className="">
            <a
                className={`relative w-14 h-14 p-2 rounded-2xl ${resolveTextColor()} transition-all duration-200 hover:cursor-pointer hover:bg-font hover:bg-opacity-25
                            active:bg-font active:bg-opacity-20`}
            >
                {children}
            </a>
        </Link>
    );
};

export default NavButton;
