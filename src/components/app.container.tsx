import SideNav from 'components/side.nav';

type Props = {
    children: JSX.Element;
};

const AppContainer: React.FC<Props> = ({ children }) => {
    return (
        <div className="flex flex-row w-screen h-screen p-20">
            <SideNav />
            <div className="flex flex-col flex-grow ml-24">{children}</div>
        </div>
    );
};

export default AppContainer;
