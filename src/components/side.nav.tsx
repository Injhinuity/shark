import {
    BookOpenIcon,
    CogIcon,
    FireIcon,
    UserGroupIcon
} from '@heroicons/react/solid';
import LogoButton from 'components/logo.button';
import NavButton from 'components/nav.button';

const SideNav: React.FC = () => {
    return (
        <div className="flex flex-col items-center space-y-12">
            <LogoButton />
            <NavButton href="/group">
                <UserGroupIcon />
            </NavButton>
            <NavButton href="/leagues">
                <FireIcon />
            </NavButton>
            <NavButton href="/wiki">
                <BookOpenIcon />
            </NavButton>
            <NavButton href="/settings">
                <CogIcon />
            </NavButton>
        </div>
    );
};

export default SideNav;
